import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    ActivityIndicator,
    FlatList,
    TouchableOpacity
} from 'react-native';
import {WebBrowser} from "expo";

export default class UsersScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            usersList: []
        };
    };

    static navigationOptions = {
        title: 'Users List',
    };

    componentDidMount() {
        setTimeout(() => {
            fetch("https://jsonplaceholder.typicode.com/users")
                .then(response => response.json())
                .then((responseJson) => {
                    this.setState({
                        loading: false,
                        usersList: responseJson
                    })
                })
                .catch(error => console.log(error)) //to catch the errors if any
        }, 2000);

    };

    renderItem=(data)=>
        <TouchableOpacity style={styles.list}>
            <Text style={styles.lightText}>{data.item.name}</Text>
            <Text style={styles.lightText}>{data.item.email}</Text>
            <Text style={styles.lightText}>{data.item.company.name}</Text>
        </TouchableOpacity>

    render() {
        if (this.state.loading) {
            return (
                <View style={styles.loader}>
                    <ActivityIndicator size="large" color="#0c9"/>
                </View>
            )
        }
        return (
            <View style={styles.container} >
                <FlatList
                    data={this.state.usersList}
                    showsVerticalScrollIndicator={false}
                    renderItem= {item=> this.renderItem(item)}
                    keyExtractor= {item=>item.id.toString()}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff"
    },
    loader: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#fff"
    },
    list: {
        paddingVertical: 4,
        margin: 5,
        backgroundColor: "#fff"
    },
    flatview: {
        justifyContent: 'center',
        paddingTop: 30,
        borderRadius: 2,
    },
    name: {
        fontFamily: 'Verdana',
        fontSize: 18
    },
    email: {
        color: 'red'
    }
});
